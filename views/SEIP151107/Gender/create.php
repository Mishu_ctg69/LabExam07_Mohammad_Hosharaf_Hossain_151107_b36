
<?php


require_once("../../../vendor/autoload.php");

use App\Message\Message;

//echo Message::getMessage();

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Gender</title>
    <link rel="stylesheet" href="../resource/css/bootstrap.min.css">
    <script src="../resource/js/bootstrap.min.js"></script>
</head>
<style>
    body{
        padding-top: 20px;
        background-color: #0f0f0f;
        background: url("../resource/img/bg1.jpg") no-repeat center center fixed;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
    }
</style>
<body>


<div class="container">
    <div class="row centered-form text-center" style="margin-top: 12%">

<!--        <h2 style="color: #a6e1ec">Create.php</h2>-->
        <div class="col-xs-12 col-sm-8 col-md-4 col-sm-offset-2 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Gender</h3>
                </div>
                <div class="panel-body">
                    <p style="color: #31b0d5;text-align: center">
                        <?php
                        echo Message::message();
                        ?>
                    </p>
                    <form role="form" action="store.php" method="post">
                        <div class="form-group">
                            <label for="tt"></label>
                            <input type="text" name="name" id="name" class="form-control input-sm" placeholder="Enter your name">
                        </div>
                        <div class="form-group" style="float: left">
                            <input type="radio" name="gender" id="gender" value="Male"> Male &nbsp;&nbsp;&nbsp;
                            <input type="radio" name="gender" id="gender" value="Female"> Female
                        </div>
                        <input type="submit" value="Create" class="btn btn-info btn-block">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
