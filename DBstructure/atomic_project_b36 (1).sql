-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 12, 2016 at 11:56 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `atomic_project_b36`
--

-- --------------------------------------------------------

--
-- Table structure for table `birthday`
--

CREATE TABLE IF NOT EXISTS `birthday` (
`birthday_id` int(10) NOT NULL,
  `name` text NOT NULL,
  `birth_date` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `birthday`
--

INSERT INTO `birthday` (`birthday_id`, `name`, `birth_date`) VALUES
(1, 'Fariha', '10/12/1991'),
(2, 'Emon', '23/10/1998'),
(3, 'dsgsdgds', '2016-11-14'),
(4, 'rteruteruty', '2016-11-08'),
(5, 'gfdgdfg', '2016-11-25'),
(6, 'dsdfsfdf', '2016-11-09'),
(7, 'jhdfjshdjfhsd', '2016-11-10'),
(8, 'trytrytr', '2016-11-30'),
(9, 'fdfgdfg', '2016-11-24'),
(10, 'ghfghfghfhgf', '2016-11-17'),
(11, 'jhjjhhk jhjh', '2016-11-23');

-- --------------------------------------------------------

--
-- Table structure for table `book_title`
--

CREATE TABLE IF NOT EXISTS `book_title` (
`book_id` int(10) NOT NULL,
  `book_title` text NOT NULL,
  `author_name` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_title`
--

INSERT INTO `book_title` (`book_id`, `book_title`, `author_name`) VALUES
(1, 'Moyurakkhi', 'Humayun Ahmed'),
(2, 'Mrinmoyir mon valo nei', 'Humayun Ahmed'),
(3, 'Kalbela', 'Somoresh Mojumder'),
(4, 'Aronyok', 'Somoresh Mojumder'),
(5, 'FDFGF', 'VCVBCV'),
(6, 'ytyy', 'nbnbnbn'),
(7, 'noori', 'gfgdfg'),
(8, '123', 'gfgdfg'),
(9, 'aqaq', 'gfgdfg'),
(10, 'fgfdgdfgd', 'eretertytyty'),
(11, 'ryyruwdhhfshd', 'sgndvcxvbcbx');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE IF NOT EXISTS `city` (
`city_id` int(10) NOT NULL,
  `name` text NOT NULL,
  `city_name` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`city_id`, `name`, `city_name`) VALUES
(1, '', 'Agrabad'),
(2, '', 'Gol Pahar'),
(3, 'Samia', 'Shariatpur'),
(4, 'SAJIB', 'Shariatpur'),
(5, 'wasif', 'Munsigonj'),
(6, 'hfjdhfscnsbgdsgh', 'Noakhali');

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE IF NOT EXISTS `email` (
`email_id` int(10) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`email_id`, `email`, `password`) VALUES
(1, 'someone@gmail.com', '7ac60b3a8bbe6eb22fc6c129d0b1d9dd'),
(2, 'example@yahoo.com', '8e0dfe712821d53026a7ba258bbc970f'),
(3, 'bristybilash16@gmail.com', '11f4d0712e8278ac1934f533e51bf6b3'),
(4, 'jdsgdfh@yahoo.com', '352751383f8e3dd5f08905fff9ee9a6e');

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE IF NOT EXISTS `gender` (
`gender_id` int(10) NOT NULL,
  `name` text NOT NULL,
  `gender` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`gender_id`, `name`, `gender`) VALUES
(1, '', 'Female'),
(2, '', 'Male'),
(3, 'derwerwe', 'Male'),
(4, 'ueyruyer bhdfdghfg', 'Female'),
(5, 'ttt', 'Female');

-- --------------------------------------------------------

--
-- Table structure for table `hobbies`
--

CREATE TABLE IF NOT EXISTS `hobbies` (
`hobby_id` int(11) NOT NULL,
  `person_name` varchar(100) NOT NULL,
  `hobbies` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hobbies`
--

INSERT INTO `hobbies` (`hobby_id`, `person_name`, `hobbies`) VALUES
(1, 'Fariha', 'Book reading,Gardening,Traveling'),
(2, 'Emon', 'Traveling'),
(3, 'ueedgfhdhvcvvxnbcv', 'Gardening,Novel Reading,Traveling'),
(4, 'ueedgfhdhvcvvxnbcv', 'Gardening,Novel Reading,Traveling'),
(5, 'rtuirutiertoiegjjbg', 'Cycling,Traveling'),
(6, 'ttt', 'Novel Reading,Traveling'),
(7, 'etrye', 'Cycling,Traveling');

-- --------------------------------------------------------

--
-- Table structure for table `profile_picture`
--

CREATE TABLE IF NOT EXISTS `profile_picture` (
`profile_pic_id` int(10) NOT NULL,
  `profile_name` varchar(100) NOT NULL,
  `profile_picture` varchar(300) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile_picture`
--

INSERT INTO `profile_picture` (`profile_pic_id`, `profile_name`, `profile_picture`) VALUES
(1, 'fariha', 'picture.jpg'),
(2, 'emon', 'pic.jpg'),
(3, 'iruiewhefhdsfbvbcnxb', '1478940477car.jpg'),
(4, 'qqqqqqqqqqqq', '1478942494image001.png'),
(5, 'hgfhfhfgh', '1478942546car.jpg'),
(6, 'cbvcbxvb', '1478942580download.jpg'),
(7, 'ttt', '1478943796car.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `summary_of_organization`
--

CREATE TABLE IF NOT EXISTS `summary_of_organization` (
`summary_organizaton_id` int(10) NOT NULL,
  `organization_name` varchar(100) NOT NULL,
  `summary` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `summary_of_organization`
--

INSERT INTO `summary_of_organization` (`summary_organizaton_id`, `organization_name`, `summary`) VALUES
(1, 'BITM', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, '),
(2, 'BASIS', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.'),
(3, 'Basis', 'yeurytueyruefhvbbvnb\r\nkjgerurtuerhgfdhbhgfgbd\r\nnfgfndjgdjfgj'),
(4, 'bitm', 'djhfsdfyueyfdhvb djhgzsdhfgshgfhds'),
(5, 'ttt', '1\r\n2\r\n3\r\n4\r\n5');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birthday`
--
ALTER TABLE `birthday`
 ADD PRIMARY KEY (`birthday_id`);

--
-- Indexes for table `book_title`
--
ALTER TABLE `book_title`
 ADD PRIMARY KEY (`book_id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
 ADD PRIMARY KEY (`city_id`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
 ADD PRIMARY KEY (`email_id`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
 ADD PRIMARY KEY (`gender_id`);

--
-- Indexes for table `hobbies`
--
ALTER TABLE `hobbies`
 ADD PRIMARY KEY (`hobby_id`);

--
-- Indexes for table `profile_picture`
--
ALTER TABLE `profile_picture`
 ADD PRIMARY KEY (`profile_pic_id`);

--
-- Indexes for table `summary_of_organization`
--
ALTER TABLE `summary_of_organization`
 ADD PRIMARY KEY (`summary_organizaton_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birthday`
--
ALTER TABLE `birthday`
MODIFY `birthday_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `book_title`
--
ALTER TABLE `book_title`
MODIFY `book_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
MODIFY `city_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
MODIFY `email_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
MODIFY `gender_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `hobbies`
--
ALTER TABLE `hobbies`
MODIFY `hobby_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `profile_picture`
--
ALTER TABLE `profile_picture`
MODIFY `profile_pic_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `summary_of_organization`
--
ALTER TABLE `summary_of_organization`
MODIFY `summary_organizaton_id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
