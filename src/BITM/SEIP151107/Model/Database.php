<?php
namespace App\Model;

use PDO;
use PDOException;




class Database
{

    public $dbh;
    public $host = "localhost";
    public $username = "root";
    public $dbname = "atomic_project_MISHU";
    public $password = "";


    public function __construct()

    {

        try
        {
            $this->dbh = new PDO("mysql:host=$this->host;dbname=$this->dbname", $this->username, $this->password);
           // echo "connection successfull";

        }
        catch (PDOException $e) {

            echo $e->getMessage();
        }

    }


}