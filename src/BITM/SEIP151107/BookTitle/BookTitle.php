<?php

namespace App\BookTitle;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;


class BookTitle extends DB
{
    public $id;
    public $title;
    public $author;

    public function __construct()
    {
        parent::__construct();
        if(!isset($_SESSION))
            session_start();
    }

    public function setData($postVaribaleData=NULL)
    {
       if(array_key_exists("id",$postVaribaleData))
       {
           $this->id = $postVaribaleData['id'];
       }
        if(array_key_exists("book_title",$postVaribaleData))
        {
            $this->title = $postVaribaleData['book_title'];
        }
        if(array_key_exists("author_name",$postVaribaleData))
        {
            $this->author = $postVaribaleData['author_name'];
        }

    }//end of set data
  /*  public function store()   //basic code
    {
        $sql = "INSERT into book_title(book_title,author_name) VALUES ('$this->title','$this->author')";
        $STH = $this->dbh->prepare($sql);
        $STH->execute();
    }//end of store*/

    public function store()
    {
        $arrData = array($this->title,$this->author);
        $sql = "INSERT into book_title(book_title,author_name) VALUES (?,?)";
        $STH = $this->dbh->prepare($sql);
        $result = $STH->execute($arrData);

        if($result)
            //Message::setMessage("Success!!Data has been inserted successfully ;)");
            Message::message("Success!!Data has been inserted successfully ;)");
        else
            //Message::setMessage("Failed!! Data has not been inserted successfully :(");
            Message::message("Failed!! Data has not been inserted successfully :(");

        Utility::redirect('create.php');

    }

}//end of book title class